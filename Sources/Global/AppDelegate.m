//
//  AppDelegate.m
//  <#App#>
//
//  Created by Martin Kiss on <#27.3.14#>.
//  Copyright (c) 2014 iAdverti s.r.o. All rights reserved.
//


#warning Rename Project, Target and Scheme (Restart Xcode to propagate all changes to files.)
#warning Fill source files headers with current date and project name.
#warning Prefix the AppDelegate class as you wish.





@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;


@end





int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}





@implementation AppDelegate





#pragma mark - Launching


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [self makeWindow];
    
    [self.window makeKeyAndVisible];
    return YES;
}


- (UIWindow *)makeWindow {
    CGRect screen = [[UIScreen mainScreen] bounds];
    UIWindow *window = [[UIWindow alloc] initWithFrame:screen];
    window.backgroundColor = [UIColor blackColor];
    window.rootViewController = [self makeRootViewController];
    return window;
}


- (UIViewController *)makeRootViewController {
    UIViewController *controller = [[UIViewController alloc] init];
    controller.view.backgroundColor = [UIColor whiteColor];
    return controller;
}





@end


